mmcore.IntegrationFactory.register(
  'SessionCam', {
   defaults: {
        isStopOnDocEnd: false
    },
    validate: function (data) {  
        if(!data.campaign)
            return 'No campaign.';
        return true;  
    },
    check: function (data) {  
      return true; 
    },
    exec: function (data) { 
        var prodSand = data.isProduction ? 'MM_Prod' : 'MM_QA';
        var mm_sc_info = data.campaignInfo.toUpperCase();
        var mm_sc_page = document.location.pathname + '_Maxymiser_' + data.campaign;
        mm_sc_page = mm_sc_page.replace(/^\//, "");

        if(!window.sessionCamRecorder) {
            if(!window.sessioncamConfiguration) {
            window.sessioncamConfiguration = new Object();
                if (data.cust_page) {
                    sessioncamConfiguration.SessionCamPageName = mm_sc_page;
                }
            }
            else {
                if (data.cust_page) {
                    if (!sessioncamConfiguration.SessionCamPageName) {
                        sessioncamConfiguration.SessionCamPageName = mm_sc_page;
                    }
                    else {
                        sessioncamConfiguration.SessionCamPageName += '_Maxymiser_' + data.campaign;
                    }
                }
            }
            //Custom Variable Code
            if (!window.sessioncamConfiguration.customDataObjects) {
                window.sessioncamConfiguration.customDataObjects = new Array();
            }
            var item = { key: "MVT", value: prodSand + '_' + mm_sc_info };
            window.sessioncamConfiguration.customDataObjects.push(item);
        }
        /*If recorder is defined set virtual page instead for custom page name*/
        else {
            if (data.cust_page) {
                if(window.sessionCamRecorder.createVirtualPageLoad) {
                    window.sessionCamRecorder.createVirtualPageLoad(mm_sc_page);
                }
            }
            //Custom Variable Code
            if(!window.sessioncamConfiguration) {
                window.sessioncamConfiguration = new Object();
            }
            if (!window.sessioncamConfiguration.customDataObjects) {
                window.sessioncamConfiguration.customDataObjects = new Array();
            }
            var item = { key: "MVT", value: prodSand + '_' + mm_sc_info };
            window.sessioncamConfiguration.customDataObjects.push(item);
        }
        
        if (data.callback) data.callback();
        return true;
    }  
});