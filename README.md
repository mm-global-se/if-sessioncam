# SessionCam

---

[TOC]

## Overview

+ This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to SessionCam.


### How we send the data?

+ Our SessionCam integration pushes the campaign experience that a user was exposed to, to a custom data object in SessionCam.

### Data format

The data sent to Adobe Analytics will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
) 

## Prerequisite

+ Campaign Name

+ Make sure that SessionCam is recording on the page. You can do this by checking the xhr calls on the console of the test page. 

+ Navigate to the page and open the developer console. Make sure that in settings "Log XMLHttpRequests" is ticked.
 
+ Interact with the test page, this should create new xhr requests from the console to SessionCam 

![sessiocam.PNG](https://bitbucket.org/repo/9y69dx/images/542304569-sessiocam.PNG)

+ If SessionCam is not recording on the page, contact `support@sessioncam.com` and ask them to whitelist your IP address for that site.


## Deployment Instructions


### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [sessioncam-register.js](https://bitbucket.org/mm-global-se/if-sessioncam/src/cfab7c042ececb1177b44d276977cadca5d0a4e3/src/sessionCam-register.js?at=master&fileviewer=file-view-default) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the SessionCam Register scripts!

+ Create a campaign script and add the [sessioncam-initialize.js](https://bitbucket.org/mm-global-se/if-sessioncam/src/cfab7c042ececb1177b44d276977cadca5d0a4e3/src/sessionCam-initialize.js?at=master&fileviewer=file-view-default) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-sessioncam#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.



## QA

### Network Tab

+ After the data is successfully sent to SessionCam you should see a new XHR requested in Network tab of the developer tools (e.g. Chrome DevTools). Under "Form Data" you should be able to see the experience data.

![SESSIONCAM_CONVER.jpg](https://bitbucket.org/repo/9y69dx/images/3088742461-SESSIONCAM_CONVER.jpg)

### DevTools Console

+ In some instances there are no requests to SessionCam in Network Tab. In this case you need to check `window.sessioncamConfiguration.customDataObjects` variable - it should contain campaign info object.

![sessioncam.png](https://bitbucket.org/repo/9y69dx/images/493215492-sessioncam.png)


### SessionCam UI

+ Log onto your SessionCam account. Under recording choose the time frame that you would like the data to be based on. "Add a filter" and choose "Field Value" from the drop down menu. Type "MVT" in the text box and wait until the drop down menu is populated with "MVT" – note this might take a few seconds. 

![sess_1.png](https://bitbucket.org/repo/9y69dx/images/3566013862-sess_1.png)

+ Choose "like" from the drop down menu (you can choose other options from this menu too). In the new text box enter the name of the campaign/ element/ variant that should have been passed onto SessionCam and click on "Filter Sessions"

![sess_2.png](https://bitbucket.org/repo/9y69dx/images/3176902784-sess_2.png)

## Common Problems

+ None currently known